/*
 * $RCSfile: lib_main.h,v $
 * $Author: swaj $
 * $Revision: 1.6 $
 */


#ifndef __LIB_MAIN_H_
#define __LIB_MAIN_H_ 1


#define DEFAULT_CDROM_DEV "/dev/cdrom"
#define FILE_MAX 1024
#define ERR_STR_MAX 512
#define DEFAULT_PLOT_FILE "cdck-plot.dat"
#define SECTOR_SIZE 2048
#define SECTOR_ERROR (-10)


class Ccdrom {

 public:
	Ccdrom ();
	Ccdrom (const char *a_cdrom_dev);
	~Ccdrom();

	const char * Error(void);
	int ReadTOC();

	void EnableInfo(bool a_enable_info);

	int OpenPlotFile (const char *plotfile = DEFAULT_PLOT_FILE);
	void WritePlotData();
	void ClosePlotFile ();

	int ReadCD ();
	void DeallocateTimings();
	void AnalyzeResults ();

 private:
	void Reset();
	bool enable_info;
	int fd;
	char cdrom_dev[FILE_MAX];
	char last_err[ERR_STR_MAX];

	char toc_header[2];

	int sectors_tot;
	int disc_status;

	FILE *pf;
	int *timings;

	void * smalloc (size_t size);
	void sfree (void*& ptr);
	void sfree (int*& ptr);

	void read_super(int offset);
	void read_super2(int offset);
	void read_super3(int offset);
	void read_super4(int offset);
	void read_super5(int offset);

	void read_creation_data (void);


	int is_isofs(void);
	int is_hs(void);
	int is_cdi(void);
	int is_cd_rtos(void);
	int is_bridge(void);
	int is_xa(void);
	int is_cdtv(void);
	int is_photocd(void);
	int is_hfs(void);
	int is_ext2(void);
	int is_ufs(void);
	int is_bootable(void);
	int is_video_cdi(void);
	int get_size(void);
	int guess_filesystem(int start_session);

	char buffer[2048];
	char buffer2[2048];
	char buffer3[2048];
	char buffer4[2048];
	char buffer5[2048];
	char buffer6[2048];

	char sbuffer[256];
	char sbuffer2[256];
	char sbuffer3[256];

	int isofs_size;
};


#endif /* __LIB_MAIN_H_ */



/*
 *  __END__
 */
