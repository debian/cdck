/*
 * $RCSfile: defines.h,v $
 * $Author: swaj $
 * $Revision: 1.1 $  
 */


#ifndef __DEFINES_H_
#define __DEFINES_H_ 1


#define RET_SUCCESS 0
#define RET_FAILED !RET_SUCCESS


#define unless(__a) if(!(__a))


typedef unsigned char uchar;
typedef void * pointer;


#ifndef MAX
#define MAX(__a,__b) ((__a) > (__b) ? (__a) : (__b))
#endif

#ifndef MIN
#define MIN(__a,__b) ((__a) < (__b) ? (__a) : (__b))
#endif


#endif /* __DEFINES_H_ */



/*
 *  __END__
 */ 

