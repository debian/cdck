#!/bin/env gnuplot

set terminal postscript
set data style lines
set grid
set xlabel 'Sectors (2Kb/sec)'
set ylabel 'Reading time (usec)'

#set logscale y

set output 'cdck-plot.ps'

plot 'cdck-plot.dat' with lines
 

#
# __END__
# 
