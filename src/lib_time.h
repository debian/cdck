/*
 * $RCSfile: lib_time.h,v $
 * $Author: swaj $
 * $Revision: 1.1 $
 */


#ifndef __LIB_TIME_H_
#define __LIB_TIME_H_ 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TV_INT_MAX_SEC 2146 // ( 2^31 - 1 ) / 1000000

#define TV_TOO_BIG (-2)
#define TV_INVALID (-3)


void time_substract_timeval (struct timeval *t2, struct timeval *t1, struct timeval *td);
float time_get_time_ms (struct timeval *tv);
int time_get_time_usec (struct timeval *tv);


#ifdef __cplusplus
}
#endif /* __cplusplus */




#endif /* __LIB_TIME_H_ */


/*
 *  __END__
 */
