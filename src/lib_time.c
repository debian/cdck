/*
 * $RCSfile: lib_time.c,v $
 * $Author: swaj $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/time.h>
#include <unistd.h>

#include "defines.h"
#include "debugging.h"
#include "cdck_main.h"
#include "lib_time.h"

//#ifndef 


void time_substract_timeval (struct timeval *t2, struct timeval *t1, struct timeval *td)
{
	if (!t2 || !t1 || !td) {
		// printf("null arg");
		return;
	}

	td->tv_sec = t2->tv_sec - t1->tv_sec;

	if (t2->tv_usec < t1->tv_usec) {
		td->tv_sec--;
		td->tv_usec = (1000000 + t2->tv_usec) - t1->tv_usec;
	} else {
		td->tv_usec = t2->tv_usec - t1->tv_usec;
	} 
}

int time_get_time_usec (struct timeval *tv)
{
	if (! tv)
		return TV_INVALID;

	if (tv->tv_sec >= TV_INT_MAX_SEC)
		return TV_TOO_BIG;

	return (tv->tv_sec * 1000000 + tv->tv_usec); 
}


float time_get_time_ms (struct timeval *tv)
{
	if (!tv)
		return 0;

	return ( (double)tv->tv_sec * 1000.0 + (double)tv->tv_usec / 1000.0);
} 



/*
 * __END__
 */
