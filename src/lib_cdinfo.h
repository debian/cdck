/*
 * $RCSfile: lib_cdinfo.h,v $
 * $Author: swaj $
 * $Revision: 1.4 $
 */

/*
 * This code is from cdinfo and has the following copyrights:
 * (c) 1996,1997  Gerd Knorr <kraxel@cs.tu-berlin.de>
 * and Heiko Eissfeldt <heiko@colossus.escape.de>
 */

#ifndef __LIB_CDINFO_H_
#define __LIB_CDINFO_H_ 1


#define FS_NO_DATA              0   /* audio only */
#define FS_HIGH_SIERRA		1
#define FS_ISO_9660		2
#define FS_INTERACTIVE		3
#define FS_HFS			4
#define FS_UFS			5
#define FS_EXT2			6
#define FS_ISO_HFS              7  /* both hfs & isofs filesystem */
#define FS_ISO_9660_INTERACTIVE 8  /* both CD-RTOS and isofs filesystem */
#define FS_UNKNOWN	       15
#define FS_MASK		       15
#define XA		       16
#define MULTISESSION	       32
#define PHOTO_CD	       64
#define HIDDEN_TRACK          128
#define CDTV		      256
#define BOOTABLE       	      512
#define VIDEOCDI       	     1024



#define PUBLISHER_OFFSET 0x13e
#define PUBLISHER_LEN 0x7f

#define PREPARER_OFFSET 0x1be
#define PREPARER_LEN 0x7f

#define SOFTWARE_OFFSET 0x23e
#define SOFTWARE_LEN 0xef



#endif /* __LIB_CDINFO_H_ */



/*
 *  __END__
 */
