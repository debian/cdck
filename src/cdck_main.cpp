/*
 * $RCSfile: cdck_main.cpp,v $
 * $Author: swaj $
 * $Revision: 1.7 $
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"
#include "debugging.h"
#include "cdck_main.h"
#include "lib_main.h"


int main (int argc, char *argv[])
{
	signed char ch;
	char *program_name = (strchr(argv[0], '/') == NULL) ? argv[0] : (strrchr(argv[0], '/') + 1);

	bool verbose = false;
	bool only_info = true;
	bool do_plot = false;
	char devname[FILE_MAX];
	char plotname[FILE_MAX];
	
	memset (devname, 0x0, FILE_MAX); 
	memset (plotname, 0x0, FILE_MAX); 

	while ((ch = getopt(argc, argv, "?hitvd:po:V"))!=-1) {
                switch (ch) {
		case 'h':
		case '?':
			print_help (program_name);
                        exit(0);
		case 'V':
			print_ver();
			exit (0);
		case 'v':
			verbose = true;
			break;
		case 'i':
			only_info = true;
			break;
		case 't':
			only_info = false;
			break;
		case 'p':
			do_plot = true;
			break;
		case 'd':
			strncpy (devname,  optarg, FILE_MAX-1); 
			break;

		case 'o':
			strncpy (plotname,  optarg, FILE_MAX-1); 
			break;

		default:
		       break;
		}

	}

	if (only_info)
		verbose=true;

	/* some debug info
	printf ("verbose = %d\n", verbose);
	printf ("only_info = %d\n", only_info);
	printf ("do_plot = %d\n", do_plot );
	printf ("devname = '%s'\n", devname);
	printf ("plotname = '%s'\n", plotname);
	*/

	Ccdrom *cdrom=0;

	if (!strlen (devname))
		cdrom = new Ccdrom ();
	else
		cdrom = new Ccdrom (devname);

	
	unless (cdrom) {
		fprintf (stderr, "Out of nemory\n");
		exit (1);
	}

	cdrom->EnableInfo (verbose);

	int rv = cdrom->ReadTOC();

	if (rv) {
		fprintf (stderr, "Unable to read TOC because %s\n", cdrom->Error() );
		delete cdrom;
		exit (1);
	}

	if (only_info)
		exit (0);


	printf ("\nNB! For disks written with some burners cdck might \n");
	printf ("    report about unreadable sectors at the end of the disk.\n");
	printf ("    In such cases you can just ignore those warnings.\n\n");

	rv = cdrom->ReadCD();
	if (rv) {
		fprintf (stderr, "Unable to read disc because %s\n", cdrom->Error() );
		delete cdrom;
		exit (1);
	}




	cdrom->AnalyzeResults();

	if (do_plot) {
		if (! strlen (plotname) )
			rv = cdrom->OpenPlotFile();
		else
			rv = cdrom->OpenPlotFile (plotname);

		if (rv) {
			fprintf (stderr, "Unable to open plot data file\n");
			delete cdrom;
			exit (1);
		}

		cdrom->WritePlotData();
		cdrom->ClosePlotFile();
	}

	delete cdrom;
	exit (0);
}

static
void print_help (char *program_name)
{
        printf ("Usage: %s [-d /dev/devname] [-i] [-v] [-p] [-o plot-file.dat]\n", program_name);
	printf ("   -d CD/DVD device name, default is /dev/cdrom\n");
        printf ("   -i Print CD/DVD information and quit, perform no timings (default mode)\n");
        printf ("   -t Perform timing test\n");
        printf ("   -p Save data for gnuplot(1) program\n");
        printf ("   -o specify plot file, ./cdck-plot.dat is default\n");
        printf ("   -V Print version\n");
        printf ("   -v Verbose operations\n");
}

static
void print_ver (void)
{
	printf ("Simple CD/DVD check program, ver %s. ", VERSION);
	printf ("Written by Alexey Semenoff (c) 2005-2008.\n");
}



/*
 * __END__
 */
