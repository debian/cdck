/*
 * $RCSfile: cdck_main.h,v $
 * $Author: swaj $
 * $Revision: 1.3 $
 */


#ifndef __CDCK_MAIN_H_
#define __CDCK_MAIN_H_ 1


int main (int argc, char *argv[]);

static void print_help (char *program_name);
static void print_ver (void);


#endif /* __CDCK_MAIN_H_ */



/* __END__ */
