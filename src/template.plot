#!/bin/env gnuplot

set data style errorlines
set terminal x11
set grid
set xlabel 'Sectors (2Kb/sec)'
set ylabel 'Reading time (usec)'

#set logscale y

plot 'cdck-plot.dat' with lines
 
pause -1


#
# __END__
# 
