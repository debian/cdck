Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cdck
Upstream-Contact: Alexey Semenoff
Source: http://swaj.net/unix/index.html#cdck
Comment:
 The upstream tarball has been repacked to remove the precompiled
 binary src/cdck (lintian flags this as an error because it gets
 cleaned) and to remove the upstream autoconf artifacts.  The package
 uses dh-autoreconf to generate these during the build.
Files-Excluded:
 src/cdck */Makefile.in
 aclocal.m4 config.guess config.sub configure depcomp install-sh
 ltmain.sh missing mkinstalldirs

Files: *
Copyright: 2005-2008, Alexey Semenoff
License: GPL-2+

Files: src/lib_cdinfo.*
Copyright:
 1996,1997, Gerd Knorr <kraxel@cs.tu-berlin.de>
 1996,1997, Heiko Eissfeldt <heiko@colossus.escape.de>
License: GPL-1+
Comment:
 https://www.ibiblio.org/pub/linux/utils/disk-management/cdinfo.c
 https://www.ibiblio.org/pub/Linux/utils/disk-management/cdinfo.lsm

Files: debian/*
Copyright:
 2006-2020, gregor herrmann <gregoa@debian.org>
 2006-2020, tony mancill <tmancill@debian.org>
License: GPL-2+

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 1, February 1989, or (at
 your option) any later version
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at your
 option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU
 General Public License can be found in `/usr/share/common-licenses/GPL-2'.
